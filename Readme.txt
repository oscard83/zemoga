Run whole set related to runner
mvn test -Dtest=com.zemoga.runners.RunSmokeTest cluecumber-report:reporting



================================================
Run specific set of test cases:
mvn test -Dtest=com.zemoga.runners.RunSmokeTest -Dcucumber.options="--tags @AppiumAndroid" cluecumber-report:reporting



================================================
Cluecumber report generated in:
target/generated-report/index.html