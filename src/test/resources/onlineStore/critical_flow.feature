@SmokeTest
Feature: Tech assessment Zemoga

@Chrome
Scenario: As client I want to buy a product
Given I am in home page
When I search for a "dress"
Then I sort the results by lowest price
Then I add the item to the shopping cart
Then I go to the shopping cart and proceed with the purchase
Then I create an account
Then I make the purchase and confirm all the information

@ChromeMobile
Scenario: As client I want to buy a product using my Android device
Given I am in home page
When I search for a "dress"
Then I sort the results by lowest price
Then I add the item to the shopping cart
Then I go to the shopping cart and proceed with the purchase
Then I create an account
Then I make the purchase and confirm all the information

@RemoteChrome
Scenario: As client I want to buy a product - SauceLabs Test Case 
Given I am in home page
When I search for a "dress"
Then I sort the results by lowest price
Then I add the item to the shopping cart
Then I go to the shopping cart and proceed with the purchase
Then I create an account
Then I make the purchase and confirm all the information

@AppiumAndroid
Scenario: As client I want to open PSVue 
Given I am in porch page