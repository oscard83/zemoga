package com.zemoga.steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.saucelabs.saucerest.SauceREST;
import com.zemoga.driver.MyDriver;
import com.zemoga.pages.CreateAccountPage;
import com.zemoga.pages.HomePage;
import com.zemoga.pages.ProductDetailPage;
import com.zemoga.util.SauceUtils;
import com.zemoga.util.SystemUtils;

import org.openqa.selenium.TakesScreenshot;

public class Stepdefs {
	
	private MyDriver driverFactory = null;
	private WebDriver driver = null;
	private SystemUtils systemUtils = null;
	private String URL;
	private HomePage homePage = null;
	private ProductDetailPage productDetailPage = null;
	private CreateAccountPage createAccountPage = null;
	private String USERNAME = null;
	private String ACCESS_KEY = null;
	private SauceREST sauceREST = null;
    private SauceUtils sauceUtils = null;
    private String sessionId = null;
	
	public Stepdefs(){
		systemUtils = new SystemUtils();
		URL = systemUtils.readPropertyKey("URL");
	}
	
	@Before("@Chrome")
	public void launchChrome() {
		driverFactory = new MyDriver("Chrome", null);
		driver = driverFactory.getDriver();
	}
	
	@Before("@ChromeMobile")
	public void launchChromeMobile() {
		driverFactory = new MyDriver("ChromeMobile", null);
		driver = driverFactory.getDriver();
	}
	
	@Before("@RemoteChrome")
	public void launchRemoteChrome(Scenario scenario) {
		driverFactory = new MyDriver("RemoteChrome", scenario.getName());
		driver = driverFactory.getDriver();
		SystemUtils sysUtils = new SystemUtils();
		USERNAME = sysUtils.readEnvVariable("SAUCE_USERNAME");
		ACCESS_KEY = sysUtils.readEnvVariable("SAUCE_KEY");
		sauceREST = new SauceREST(USERNAME, ACCESS_KEY);
        sauceUtils = new SauceUtils(sauceREST);
		this.sessionId = ((RemoteWebDriver)this.driver).getSessionId().toString();
	}
	
	@Before("@AppiumAndroid")
	public void launchLocalAndroid(Scenario scenario) {
		driverFactory = new MyDriver("AppiumAndroid", scenario.getName());
		driver = driverFactory.getDriver();
	}
	
	@Given("^I am in home page$")
    public void i_am_in_home_page() {
		driver.get(URL);
		homePage = new HomePage(driver);
		Assert.assertTrue(homePage.verifyHomeLoaded());
    }
	
	@Given("^I am in porch page$")
    public void i_am_in_porch_page() {
	}
    
	@When("^I search for a \"(.*)\"$")
    public void i_am_in_home_page(String item) {
		homePage = new HomePage(driver);
		Assert.assertTrue(homePage.searchItemtoBuy(item));
    }
	
	@Then("^I sort the results by lowest price$")
	public void i_sort_the_results_by_lowest_price() {
		homePage = new HomePage(driver);
		Assert.assertTrue(homePage.orderResults());
    }
	
	@Then("^I add the item to the shopping cart$")
	public void i_add_the_item_to_the_shopping_cart() {
		Assert.assertTrue(homePage.openProductDetail());
		productDetailPage = new ProductDetailPage(driver);
		Assert.assertTrue(productDetailPage.addProductToCart());
    }
	
	@Then("^I go to the shopping cart and proceed with the purchase$")
	public void i_go_to_the_shopping_cart_and_proceed_with_the_purchase() {
		Assert.assertTrue(productDetailPage.openCart());
		Assert.assertTrue(productDetailPage.proceedToCheckout());
    }
	
	@Then("^I create an account$")
	public void i_create_an_account() {
		createAccountPage = new CreateAccountPage(driver);
		Assert.assertTrue(createAccountPage.createAccount());
    }
	
	@Then("^I make the purchase and confirm all the information$")
	public void i_make_the_purchase_and_confirm_all_the_information() {
		Assert.assertTrue(createAccountPage.confirmPurchase());
    }
	
	//@After   
	public void endTest(Scenario scenario) {
		
		if(scenario.isFailed()){
			final byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshot, "image/png");
		}
		
		if(this.sessionId!=null)
			sauceUtils.updateResults(!scenario.isFailed(), sessionId);
		
		if(driver!=null){
			driver.close();
			driver.quit();
		}
	}	
}