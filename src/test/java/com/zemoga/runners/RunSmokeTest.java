package com.zemoga.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(	plugin = {"pretty","html:target/report","json:target/cucumber-report/cucumber.json"},
					features = {"src/test/resources/onlineStore/critical_flow.feature"},
					glue = {"classpath:com/zemoga/steps"},
					tags = {"@AppiumAndroid"})

public class RunSmokeTest {
}