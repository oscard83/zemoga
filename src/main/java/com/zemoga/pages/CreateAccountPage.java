package com.zemoga.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;


public class CreateAccountPage extends BasePage{

	@FindBy(id = "email_create")
	private WebElement emailField;
	
	@FindBy(id = "SubmitCreate")
	private WebElement createAccountButton;
	
	@FindBy(xpath = "//*[@id='account-creation_form']/div[1]/h3")
	private WebElement personalInfoLabel;
	
	@FindBy(id = "id_gender1")
	private WebElement mrRadioBtn;
	
	@FindBy(id = "customer_firstname")
	private WebElement firstnameField;
	
	@FindBy(id = "customer_lastname")
	private WebElement lastnameField;
	
	@FindBy(id = "email")
	private WebElement emailSecondField;
	
	@FindBy(id = "passwd")
	private WebElement passwordField;
	
	@FindBy(id = "days")
	private WebElement selectDays;
	
	@FindBy(id = "months")
	private WebElement selectMonths;
	
	@FindBy(id = "years")
	private WebElement selectYears;
	
	@FindBy(id = "firstname")
	private WebElement addressFirstnameField;
	
	@FindBy(id = "lastname")
	private WebElement addressLastnameField;
	
	@FindBy(id = "address1")
	private WebElement address1Field;
	
	@FindBy(id = "city")
	private WebElement cityField;
	
	@FindBy(id = "id_state")
	private WebElement stateSelect;
	
	@FindBy(id = "postcode")
	private WebElement postcodeField;
	
	@FindBy(id = "phone_mobile")
	private WebElement phoneMobileField;
	
	@FindBy(id = "alias")
	private WebElement aliasField;
	
	@FindBy(id = "submitAccount")
	private WebElement submitAccountField;
	
	@FindBy(xpath = "//*[@id='center_column']/h1")
	private WebElement labelAddress;
	
	@FindBy(xpath = "//*[@id='center_column']/form/p/button")
	private WebElement proceedToCheckoutButton;
	
	@FindBy(id = "cgv")
	private WebElement termsConditionsCheck;
	
	@FindBy(xpath = "//*[@id='carrier_area']/h1")
	private WebElement labelShipping;
	
	@FindBy(xpath = "//*[@id='form']/p/button")
	private WebElement proceedToCheckoutButton2;
	
	@FindBy(xpath = "//*[@id='center_column']/h1")
	private WebElement labelPayment;
	
	@FindBy(xpath = "//*[@id='HOOK_PAYMENT']/div[1]/div/p/a")
	private WebElement paymentOptionButton;
	
	@FindBy(xpath = "//*[@id='center_column']/h1")
	private WebElement labelSummary;
	
	@FindBy(xpath = "//*[@id='center_column']/form/div/h3")
	private WebElement answerTitle;
	
	@FindBy(xpath = "//*[@id='center_column']/form/div/p[1]")
	private WebElement paragraph1;
	
	@FindBy(xpath = "//*[@id='center_column']/form/div/p[2]")
	private WebElement paragraph2;
	
	@FindBy(xpath = "//*[@id='center_column']/form/div/p[3]")
	private WebElement paragraph3;
	
	@FindBy(xpath = "//*[@id='center_column']/form/div/p[4]")
	private WebElement paragraph4;
	
	public CreateAccountPage(WebDriver driver) {
		super(driver);
	}
	
	public boolean createAccount(){
		String mail = df.getEmailAddress();
		emailField.sendKeys(mail);
		createAccountButton.click();
		By labelll = By.xpath("//*[@id='account-creation_form']/div[1]/h3");
		wait.until(ExpectedConditions.presenceOfElementLocated(labelll));
				
		mrRadioBtn.click();
		firstnameField.sendKeys(df.getFirstName());
		lastnameField.sendKeys(df.getLastName());
		emailSecondField.clear();
		emailSecondField.sendKeys(mail);
		passwordField.sendKeys("123456789");
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", selectDays);
		Select dropDownDays = new Select(selectDays);
		dropDownDays.selectByValue("20");
		Select dropDownMonths = new Select(selectMonths);
		dropDownMonths.selectByValue("7");
		Select dropDownYears = new Select(selectYears);
		dropDownYears.selectByValue("1983");
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", addressFirstnameField);
		address1Field.sendKeys(df.getAddress());
		cityField.sendKeys(df.getCity());
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", stateSelect);
		Select dropDownStates = new Select(stateSelect);
		dropDownStates.selectByValue("6");
		postcodeField.sendKeys(new Integer(df.getNumberBetween(11111, 99999)).toString());
		phoneMobileField.sendKeys("12346677883");
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", submitAccountField);
		submitAccountField.click();
		wait.until(ExpectedConditions.visibilityOf(labelAddress));
		
		proceedToCheckoutButton.click();
		wait.until(ExpectedConditions.visibilityOf(labelShipping));
		
		termsConditionsCheck.click();
		proceedToCheckoutButton2.click();
		
		wait.until(ExpectedConditions.visibilityOf(labelPayment));
		if(labelPayment.getText().contains("PLEASE CHOOSE YOUR PAYMENT METHOD"))
			return true;
		else
			return false;
	}
	
	public boolean confirmPurchase(){
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();",paymentOptionButton);
		paymentOptionButton.click();
		wait.until(ExpectedConditions.visibilityOf(labelSummary));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();",labelSummary);
		
		if(answerTitle.getText().contains("BANK-WIRE PAYMENT") &&
		   paragraph1.getText().contains("You have chosen to pay by bank wire. Here is a short summary of your order") &&
		   paragraph2.getText().contains("The total amount of your order comes to:") &&
		   paragraph3.getText().contains("We allow the following currency to be sent via bank wire: Dollar") &&
		   paragraph4.getText().contains("Please confirm your order by clicking"))
			return true;
		else
			return false;
	}
}