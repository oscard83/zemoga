package com.zemoga.pages;

import java.util.Random;
import org.fluttercode.datafactory.impl.DataFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

/*
 * 
 * */


public class BasePage {
	protected WebDriver driver;	
	protected WebDriverWait wait;
	protected DataFactory df;

	public BasePage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
		wait = new WebDriverWait(this.driver, 200);
		df = new DataFactory();
		Random aleatorio = new Random(System.currentTimeMillis());
		df.randomize(aleatorio.nextInt(999999));
	}
	
	protected WebDriver getDriver() {
		return driver;
	}

	public WebDriverWait getWait() {
		return wait;
	}
	
	public void dispose(){
		if(driver!=null)
			driver.close();
	}
}