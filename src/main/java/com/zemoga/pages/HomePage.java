package com.zemoga.pages;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class HomePage extends BasePage{

	@FindBy(id = "search_query_top")
	private WebElement searchField;
	
	@FindBy(xpath = "//*[@id='searchbox']/button")
	private WebElement searchButton;
	
	@FindBy(xpath = "//*[@id='center_column']/ul")
	private WebElement searchResults;
	
	@FindBy(css = "h1.page-heading.product-listing")
	private WebElement labelSearchListing;
	
	@FindBy(css = "span.heading-counter")
	private WebElement resultsCounterLabel;
	
	@FindBy(id = "selectProductSort")
	private WebElement selectOrderResults;
	
	@FindBy(xpath = "//*[@id='columns']/div[2]")
	private WebElement backToSearchLink;
	
	private List<WebElement> results = null;
	
	public HomePage(WebDriver driver) {
		super(driver);
	}
	
	public boolean verifyHomeLoaded(){
		
		if(searchField.isEnabled() &&
		searchButton.isEnabled())
			return true;
		else
			return false;
	}
	
	public boolean searchItemtoBuy(String item){
		searchField.sendKeys(item);
		searchButton.click();		
		wait.until(ExpectedConditions.visibilityOf(labelSearchListing));
		String[] parts = resultsCounterLabel.getText().split(" ");
		int resultsNumber = Integer.parseInt(parts[0]);
		
		if(resultsNumber > 0)
			return true;
		else
			return false;
	}
	
	public boolean orderResults(){
		Select dropDown = new Select(selectOrderResults);
		dropDown.selectByVisibleText("Price: Lowest first");
		wait.until(ExpectedConditions.visibilityOf(labelSearchListing));
		results = searchResults.findElements(By.xpath("li"));
		List<Double> prices = new ArrayList<Double>();
		
		for (WebElement temp : results) {
			List<WebElement> discount = temp.findElements(By.xpath("div/div[2]/div[1]/span[2]"));
			String stringTemp = null;
			
			if(discount.size() > 0)
				stringTemp = discount.get(0).getText();
			else
				stringTemp = temp.findElement(By.xpath("div/div[2]/div[1]/span")).getText();			
			prices.add(Double.parseDouble(stringTemp.replace("$", "")));
		}
		
		List<Double> orderedPrices = prices;
		orderedPrices.sort(Comparator.naturalOrder());
		
		int index = 0;
		for (Double tempPrice : orderedPrices) {
			if(!tempPrice.equals(prices.get(index)))
				return false;
			index++;
		}
		return true;
	}
	
	public boolean openProductDetail(){
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", results.get(0));
		Actions action = new Actions(driver);
		action.moveToElement(results.get(0)).click(results.get(0).findElement(By.xpath("div/div[2]/div[2]/a[2]"))).build().perform();
		wait.until(ExpectedConditions.visibilityOf(backToSearchLink));
		
		if(backToSearchLink.isEnabled())
			return true;
		else
			return false;
	}
}