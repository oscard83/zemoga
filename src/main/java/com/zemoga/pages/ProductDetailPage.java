package com.zemoga.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ProductDetailPage extends BasePage{

	@FindBy(xpath = "//*[@id='add_to_cart']/button")
	private WebElement addToCartButton;
	
	@FindBy(id = "cart_title")
	private WebElement cartTitleLabel;
	
	@FindBy(xpath = "//*[@id='center_column']/div/div/div[3]/h1")
	private WebElement productTitleLabel;
	
	@FindBy(xpath = "//*[@id='layer_cart']/div[1]/div[1]/h2")
	private WebElement productInCartLabel;
	
	@FindBy(xpath = "//*[@id='layer_cart']/div[1]/div[2]/div[4]/a")
	private WebElement proceedToCheckoutButton;
	
	@FindBy(xpath = "//*[@id='center_column']/p[2]/a[1]")
	private WebElement proceedToCheckoutSecondPartButton;
	
	@FindBy(xpath = "//*[@id='center_column']/h1")
	private WebElement authLabel;
	
	public ProductDetailPage(WebDriver driver) {
		super(driver);
	}
	
	public boolean addProductToCart(){
		wait.until(ExpectedConditions.visibilityOf(productTitleLabel));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", addToCartButton);
		addToCartButton.click();
		wait.until(ExpectedConditions.visibilityOf(productInCartLabel));
		if(productInCartLabel.getText().contains("Product successfully added to your shopping cart"))
			return true;
		else
			return false;
	}
	
	public boolean openCart(){
		proceedToCheckoutButton.click();
		
		if(cartTitleLabel.isDisplayed())
			return true;
		else
			return false;
	}
	
	public boolean proceedToCheckout(){
		proceedToCheckoutSecondPartButton.click();
		wait.until(ExpectedConditions.visibilityOf(authLabel));
		
		if(authLabel.getText().contains("AUTHENTICATION"))
			return true;
		else
			return false;
	}
}