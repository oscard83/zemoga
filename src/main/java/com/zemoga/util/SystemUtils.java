package com.zemoga.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.exec.CommandLine;

public class SystemUtils {
	
	public String readEnvVariable(String var) {
		
		if(var.length() > 0) {
			return System.getenv(var);
		}
		else
			return null; 
	}
	
	public String readPropertyKey(String key) {
		InputStream filesReader = null;
		Properties prop = null;
		
		try {
			filesReader = new FileInputStream("config.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		prop = new Properties();
		
		try {
			prop.load(filesReader);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return prop.getProperty(key);
	}
	
	public void startAppium(String appiumPort, String chromeDriverPort, String selendroidPort, String bootstrapPort) {
		if (System.getProperty("os.name").toLowerCase().indexOf("mac") > -1)
			startAppiumMac(appiumPort, chromeDriverPort, selendroidPort, bootstrapPort);
	}
	
	private void startAppiumMac(String appiumPort, String chromeDriverPort, String selendroidPort, String bootstrapPort) {
		CommandLine command = new CommandLine("appium");
		command.addArgument("-a");
		command.addArgument("127.0.0.1");
		command.addArgument("-p");
		command.addArgument(appiumPort);
		command.addArgument("--selendroid-port");
		command.addArgument(selendroidPort);
		command.addArgument("-bp");
		command.addArgument(bootstrapPort);
		command.addArgument("--chromedriver-port");
		command.addArgument(chromeDriverPort);
		command.addArgument("--platform-name");
		command.addArgument("Android");
		command.addArgument("--device-ready-timeout");
		command.addArgument("30");
		
		Runtime runtime = Runtime.getRuntime();
		try {
			runtime.exec(command.toString());
			Thread.sleep(10000);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
}
