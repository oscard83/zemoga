package com.zemoga.driver;

import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

//SauceLabs implementation
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.zemoga.util.SystemUtils;
import java.net.MalformedURLException;
import java.net.URL;


public class MyDriver {
	private WebDriver driver = null;	
	private String USERNAME = null;
	private String ACCESS_KEY = null;
	
	public MyDriver(String browser, String scenarioName){
		
		SystemUtils sysUtils = new SystemUtils();
		USERNAME = sysUtils.readEnvVariable("SAUCE_USERNAME");
		ACCESS_KEY = sysUtils.readEnvVariable("SAUCE_KEY");
		String URL = "http://"+USERNAME+":"+ACCESS_KEY+"@ondemand.saucelabs.com:80/wd/hub";
		String URLAppium = "http://127.0.0.1:4723/wd/hub";
		
		switch(browser){
			case "Firefox":
				System.setProperty("webdriver.gecko.driver", sysUtils.readPropertyKey("gecko.driver"));
				driver = new FirefoxDriver();
				break;
		
			case "Chrome":
				System.setProperty("webdriver.chrome.driver", sysUtils.readPropertyKey("chrome.driver"));
				driver = new ChromeDriver();
				break;
				
			case "ChromeMobile":
				System.setProperty("webdriver.chrome.driver", sysUtils.readPropertyKey("chrome.driver"));
				Map<String, String> mobileEmulation = new HashMap<>();
				mobileEmulation.put("deviceName", "Nexus 5X");
				ChromeOptions chromeOptions = new ChromeOptions();
				chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
				driver = new ChromeDriver(chromeOptions);
				break;
				
			case "RemoteChrome":
				DesiredCapabilities caps = DesiredCapabilities.chrome();
				caps.setCapability("platform", "macOS 10.13");
				caps.setCapability("version", "72.0");
				caps.setCapability("screenResolution", "1400x1050");
				caps.setCapability("name", scenarioName);
				caps.setCapability("tags", "Tag1");
				caps.setCapability("build", "build-1234");
				
				try {
					driver = new RemoteWebDriver(new URL(URL), caps);
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
				break;
				
			case "AppiumAndroid":
				DesiredCapabilities capsAppiumAndroid = DesiredCapabilities.android();
				
				capsAppiumAndroid.setCapability("dontStopAppOnReset", true);
				capsAppiumAndroid.setCapability("enablePerformanceLogging", true);
				capsAppiumAndroid.setCapability("platformVersion", "5.0.1");
				capsAppiumAndroid.setCapability("platformName", "Android");
				capsAppiumAndroid.setCapability("--suppress-adb-kill-server", "true");
				capsAppiumAndroid.setCapability("noReset", "true");
				capsAppiumAndroid.setCapability("automationName", "Appium");
				capsAppiumAndroid.setCapability("newCommandTimeout", 60 * 5 * 60);
				capsAppiumAndroid.setCapability("deviceName", sysUtils.readPropertyKey("Address_device1"));
				capsAppiumAndroid.setCapability("udid", sysUtils.readPropertyKey("Address_device1"));
				capsAppiumAndroid.setCapability("app", System.getProperty("user.dir")+sysUtils.readPropertyKey("appLocation"));
				//capsAppiumAndroid.setCapability("appPackage", sysUtils.readPropertyKey("appPackage"));
				capsAppiumAndroid.setCapability("appActivity", sysUtils.readPropertyKey("appActivity"));
				
				try {
					driver = new RemoteWebDriver(new URL(URLAppium), capsAppiumAndroid);
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
				break;
			}
	}
	
	public WebDriver getDriver(){
		return this.driver;
	}
}